![Logo](docs/images/logo.png)

# Clean Contacts Documentation #

## About ##
* Clean Contacts scans your Dynamics 365 contact data and identifies whether a person has passed away or moved address, and if known, the solution will provide their new address
* Out of the box, it scans contacts (and [leads](docs/flow-leads.md) and [accounts](docs/flow-accounts.md)) but can easily be configured to scan any other name and address table
* It can be simply configured to accommodate Microsoft api limits (see [Continuous Cleaning](docs/flow-continuous-cleaning.md))


## Quick Start ##
* Register at: [https://portal.clean-contacts.co.uk](https://portal.clean-contacts.co.uk)
* After logging in, from the home page:
    * Copy your ClientId & Client Secret
        * Save both of these somewhere safe (a password vault etc)
    * Download 'Clean Contacts' and 'Clean Contacts Sales Hub' solutions
* [Import Clean Contacts Solutions](docs/solution-import.md) into Dynamics Powerapps
* [Enable Clean Contacts flow](docs/flow-setup.md)

## Recommendations/Best practices ##
* [Copy the flow from the Clean Contacts managed solution](docs/flow-setup.md)
* Leave the solution flow switched off; this will allow future upgrades to the solution
* Edit the copy
* When running multiple flows; make sure to [use separate environment variables for each flow](docs/flow-multiple.md)

## Contents ##
* [How to import the Clean Contacts solution](docs/solution-import.md)
* [What's included in the Clean Contacts solution](docs/solution-content.md)
* [Why are there 2 solutions](docs/solution-content.md)
* [How to setup/enable the flow](docs/flow-setup.md)
* [How to test the flow](docs/flow-testing.md)
* [How to view/change Clean Contacts configuration (environment variables)](docs/solution-config.md)
* [How does the flow work](docs/flow.md)
* [How to remove the approval step](docs/flow-remove-approval.md)
* [How to run a single service ie. passed away](docs/customise-service.md)
* [How to run against the lead table](docs/flow-leads.md)
* [How to run against the account table](docs/flow-accounts.md)
* [How to change the flow input criteria/view](docs/flow-custom-view.md)
* [How to run against a custom table](docs/flow-custom-table.md)
* [How to run Clean Contacts flow on a timer/schedule](docs/flow-schedule.md)
* [What is Continuous Cleaning](docs/flow-continuous-cleaning.md)
* [How to run multiple Clean Contacts flows](docs/flow-multiple.md)
* [How to upgrade Clean Contacts solution](docs/solution-upgrade.md)
* [Releases/Versions](docs/versions.md)

## Clean Contacts Sales Hub ##
* [How to apply the 'Moved To' address](docs/moved-to-flow.md)

## Demo Videos ##
* [Introduction to Clean Contacts](https://www.youtube.com/watch?v=jGf6L_isKr8)
* [Demo of the Solution](https://www.youtube.com/watch?v=1F6w5Em1btE)
* [Importing the solution](https://www.youtube.com/watch?v=ZknIXY-AFFI)

## FAQs ##
* [Frequently asked questions](docs/faqs.md)

## Contact Us ##
* If you need to [contact us](https://portal.clean-contacts.co.uk/contact-us), please have your clientId (and jobId, if relevant)