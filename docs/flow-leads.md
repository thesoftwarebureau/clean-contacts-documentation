![Logo](images/logo.png)

# How to clean Leads #

## How to customise the flow to clean lead records ##
* Edit the flow
* Click the step: 'Create Clean Contacts Job and Validate View'
* Edit the step
    1. Remove the 'MovedTo' service
    2. Change the InputViewName to 'Clean Contacts Input Leads'
    ![customise-service](images/flow-leads.png)
* Save the flow
* [Be aware if you are running multiple flows](flow-multiple.md)
