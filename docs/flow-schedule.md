![Logo](images/logo.png)

# Schedule Clean Contacts Flow #

## How to customise the flow to run on a schedule ##
* Edit the flow
* Click the trigger step: 'Manually trigger a flow'
* Delete the trigger step
    1. Click ...
    2. Click Delete
    ![customise-service](images/flow-schedule-1.png)
* Confirm the step deletion
* You are now prompted for a new trigger step
* type 'schedule' into the search box & click the 'Recurrence Schedule' icon
    ![customise-service](images/flow-schedule-2.png)
* Set when you wish to run the flow
    * The example shows every Saturday at 1pm
    ![customise-service](images/flow-schedule-3.png)
* Save the flow and enable it by clicking 'Turn On'
