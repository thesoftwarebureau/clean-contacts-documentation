![Logo](images/logo.png)

# Customise Flow to clean a custom table #

## Overview ##
* Out of the box, Clean Contacts ships with a single flow configured to clean the contacts table
* It can be customised to clean the [leads table](flow-leads.md) and [accounts table](flow-accounts.md) very easily, as we ship a view defining the input fields required for the leads and accounts table

## Customisation ##
* The input rows to the Clean Contacts flow are defined by a system view
* The default view for the contacts table shipped with Clean Contacts is called: 'Clean Contacts Input Contacts'
    * It can be found within the solution, contacts entity/views
* You cannot change 'Clean Contacts Input Contacts' (as it is within a managed solution) but you can create a new system view that you can then use in the Clean Contacts flow
* A new view can be based upon any existing table e.g. customer
* Any custom table used must contain the custom match columns

## Customisation Steps ##
1. Add Clean Contact custom match fields to your custom table
2. Create new view based on custom table
3. Create a flow using the new view
    * [Be aware if you are running multiple flows](flow-multiple.md)


## Add Clean Contact custom match fields to your custom table ##
* **Navigate to the Clean Contact solution**
![customise flow view](images/flow-custom-columns-1.png)

* **Copy the flow 'Clean Contacts Create Table Custom Columns'**
![customise flow view](images/flow-custom-columns-2.png)

* **Navigate to 'My Flows' and edit your copy**
![customise flow view](images/flow-custom-columns-3.png)

* **Edit the step 'Initialize variable: TableName'**
    1. enter your table name as the value
![customise flow view](images/flow-custom-columns-4.png)

* **Then**
    * Save the flow
    * back out to the previous page
    * enable the flow
    * run the flow

* NB the duration of the flow will depend upon how many records there are in the table


## Create a custom flow input view ##
* [Create a custom input view](flow-custom-view.md)
