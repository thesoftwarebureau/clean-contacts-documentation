![Logo](images/logo.png)

# Clean Contacts Sales Hub #

## Update single contact address with the 'Moved To' address ##

* **Navigate to the contact whom has been flagged with 'Moved To' Method 1**
* **Select 'Apply Moved To' button**
    ![customise-service](images/moved-to-button-single-1.png)
* **From the pop-up confirmation: click 'Apply' button**
* **Result**
    ![customise-service](images/moved-to-button-single-2.png)


* **Navigate to the contact whom has been flagged with 'Moved To' Method 2**
* **Select 'Flow' menu**
    ![customise-service](images/moved-to-flow-single-1.png)
* **Select flow 'Update Contact Moved To Address'**
    ![customise-service](images/moved-to-flow-single-2.png)
* **Click 'Run Flow' (you will need to accept connections the first time)**
* **Result (after a page refresh)**
    ![customise-service](images/moved-to-flow-single-3.png)


## Update multiple contact addresses with the 'Moved To' address ##

* **Navigate to the Clean Contacts dashboard**
* **Scroll to the 'Moved To' contacts**
* **Select the contacts**
    ![customise-service](images/moved-to-flow-multiple-1.png)
* **Select 'Flow' menu**
    ![customise-service](images/moved-to-flow-multiple-2.png)
* **Select flow 'Update Contact Moved To Address'**
    ![customise-service](images/moved-to-flow-multiple-3.png)
* **Click 'Run Flow' (you will need to accept connections the first time)**