![Logo](images/logo.png)

# Importing Clean Contacts Solutions #
* Navigate to: [https://make.powerapps.com](https://make.powerapps.com)
* Select your Environment
* Select 'Solutions' [1]
* Click 'Import' [2]
    * ![cdx](images/import-solution-1.png)
* Select Clean Contacts Solution Zip file
    * Note: you must install the Clean Contacts Solution before the Contacts Sales Hub Solution
* Select connections when prompted

* Enter your clientId & client secret:
    * ![cdx](images/import-solution-client-id.png)
    * Click 'import'
* Wait for the solution to import (usually about 3-5 minutes)
    * ![cdx](images/import-solution-completed.png)
* Import Clean Contacts Sales Hub Solution Zip file
