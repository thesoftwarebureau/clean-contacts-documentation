![Logo](images/logo.png)

# Clean Contacts Configuration #

## About ##
* Clean Contacts uses [Environment variables](https://docs.microsoft.com/en-us/powerapps/maker/common-data-service/environmentvariables) to store required configuration.


## Environment Variables ##
| Env var name | Description |
| ----------- | ----------- |
| tsb_clientid | your client id |
| tsb_clientsecret | your client secret |
| tsb_apiurl | api url.  Default: https://suppress.swiftcore.net/api/v2.0/crm  |
| tsb_idserverurl | OAuth server url.  Default: https://id.swiftcore.net |
| tsb_chunksize | Chunk Size, the number of rows in a batch submission.  For api limit efficiency, set the chunk size to be a multiple of 100. Default:500  Min:1  Max:2000 |
| tsb_lastrecordid | Used to remember the last record id that was submitted for cleaning. Default: 0 |


## Edit a Clean Contacts Environment Variable ##
Clean Contacts is a managed solution. Currently, managed solution environment variables cannot be edited using the default solution method (as detailed below).  So included as part of the Clean Contacts Solution is a flow: 'Clean Contacts Update Environment Variable'.  This flow allows you to update an existing managed solution environment variable.

### Clean Contacts Update Environment Variable Flow Usage ###
* **From within the Clean Contacts Solution, click the flow:**
![select flow](images/solution-config-env-var-select-flow.png)

* **Click 'Save as' to copy the flow**
    * ![save as](images/solution-config-env-var-flow-save-as.png)
    * ![accept connections](images/solution-config-env-var-flow-accept-connections.png)
    * ![name the flow copy](images/solution-config-env-var-flow-name-the-copy.png)
* **Navigate and edit the copy**
![navigate to copy](images/solution-config-env-var-navigate-to-copy.png)

* **Set the Variable name and Value**
![edit the flow](images/solution-config-env-var-edit-flow.png)

* **Run the flow**
    * 'Turn On' the flow
    * 'Run' the flow
    * If successful, then the environment variable has been updated


## List/View Environment Variables ##

* **Navigate to [https://make.powerapps.com](https://make.powerapps.com)**
* **Navigate to Solutions** ![Solutions](images/solution-config-solutions.png)
* **Navigate to Default Solution** ![Default solution](images/solution-config-default-solution.png)
* **Filter to Environment Variables** ![Env Vars Filter](images/solution-config-env-var-filter.png)
**The list of environment variables will be displayed: ![List of Variables](images/solution-config-env-var-list.png)**



## Edit an Environment Variable ##
1. Click the Environment Variable you want to edit
2. Edit the 'Current Value'
3. Click 'Save'

![Edit env value](images/solution-config-env-edit.png)



## Create a New Environment Variable ##
1. Set the filter to 'All'
2. Click 'New'
3. Select 'Environment Variable'
![New env var](images/solution-config-env-var-new.png)


**Enter variable details**

1. Enter the display name
    * Note down the schema name as this is what the flow will need (***new_MyFlowLastRecordId***)
2. Enter description
3. Select 'Text' for the data type
4. Set the default value to 0
5. Set the current value to 0
6. Click the save button
![New env var](images/solution-config-env-var-new-detail.png)

