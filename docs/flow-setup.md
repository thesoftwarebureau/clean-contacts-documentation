![Logo](images/logo.png)

# Clean Contacts Flow Initial Setup #

## About ##
* This is a walk through of how to copy and enable the Clean Contacts flow.
* NB it is important not to edit the flow within the solution as that will prevent any future solution updates.

## Walk Through ##
* **From within the Clean Contacts Solution, click the flow:**
![select flow](images/flow-setup-select-flow.png)

* **Click 'Save as' to copy the flow**
    * ![save as](images/flow-setup-save-as.png)
    * ![accept connections](images/flow-setup-accept-connections.png)
    * ![name the flow copy](images/flow-setup-name-the-copy.png)
* **Navigate to the copy**
![navigate to copy](images/flow-setup-navigate-to-copy.png)

* **Click 'Edit' to edit the flow**
![edit the flow](images/flow-setup-edit-flow.png)

* **Update the Teams step**
    * Change the Team & Channel
![update teams step](images/flow-setup-edit-teams-step.png)

* **Update the Approval step**
    * Change the assigned to email address
![update approval step](images/flow-setup-edit-approval-step.png)

* **Save the flow changes; Click:**
     1. 'Save'
     2. and then the 'back arrow' to close the editor
![save flow](images/flow-setup-save-flow.png)

* **'Turn On' the flow**
    * ![turn flow on](images/flow-setup-turn-flow-on.png)
    * ![flow is on](images/flow-setup-your-flow-is-on.png)


## All ready to run ##
:smiley:

## Notes ##
* 'Connections' should now list
    * Approvals
    * Common Data Service
    * Microsoft Teams
