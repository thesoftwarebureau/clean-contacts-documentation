![Logo](images/logo.png)


# Solution Upgrade #
* The Clean Contacts solution can only be upgraded if none of the artefacts (flow, plugin etc) within the solution have been changed
* It is also recommended that any flows within the Clean Contacts solution are turned off
