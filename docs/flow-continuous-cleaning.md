![Logo](images/logo.png)

# Continuous Cleaning #

## What is Continuous Cleaning ? ##
* Continuous Cleaning was conceived to allow cleaning within power automate api limits
* The idea is that the flow runs once a day and cleans x number of rows, setting x below your api limit
* Tomorrow's flow run will start where the last run finished

## How do I set up continuous Cleaning ? ##
* make a copy of 'Clean Contacts Flow'
* edit the 'Initialize variable: MaxRecordsToRead' step and set the MaxRecordsToRead value to the number of rows to clean (per run/day)
    ![set MaxRecordsToRead](images/flow-max-records-to-read.png)

* edit the copy and [change the trigger](flow-schedule.md) to run on a schedule (daily)
* [remove the approval step](flow-remove-approval.md)

## How many rows should we clean each day ? ##
* This will be trial and error :anguished:
* Start with MaxRecordsToRead to be half of your daily api limit

## Running multiple flows ##
* **Important:** To run multiple flows then each flow must have its own environment variable to remember the lastRecordId
* [How to run multiple Clean Contacts flows](flow-multiple.md)
