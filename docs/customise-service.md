![Logo](images/logo.png)

# Customise Service(s) #

## Overview ##
* Clean Contacts has 3 services which can be run individually or in combination

## Services ##
| Service | Description |
| ----------- | ----------- |
| Passed Away | Returns a flag indicating whether the contact has passed away |
| Moved Away | Returns a flag indicating whether the contact has moved address |
| Moved To | Returns the new address to which the contact has moved to (not available for leads) |

## How to customise the flow to run only the passed away service ##
* Edit the flow
* Click the step: 'Create Clean Contacts Job and Validate View'
* Change Service from: 'PassedAway,MovedAway,MovedTo' to 'PassedAway' ![customise-service](images/customise-service.png)