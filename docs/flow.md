![Logo](images/logo.png)

# Clean Contacts Flow #

## About ##
* The Clean Contacts Flow controls all aspects of cleaning your data

## Summary ##
* Setup
* Read Dynamics data & submit for cleaning
* Get match report
* Update matching Dynamics rows

## Detail ##
* **Setup**
    * Set the required [Clean Contacts service(s)](customise-service.md)
    * Set the [view](flow-custom-table.md), the filter determining the rows to be cleaned
    * The service and view name can be customised
 ![job and view](images/flow-job-and-view.png)
* **Custom Column Check**
    * Will create (if missing) the match columns for the table that the view is based upon
    * This step (and associated variable steps) should not be changed
![job and view](images/flow-custom-columns.png)
* **Submit Chunks**
    * Reads through Dynamics data 500 rows at a time
    * NB.
        * 500 is the default and held in the environment variable: tsb_chunksize
        * do not change this setting unless asked by support
    * Submits the 500 names and addresses (a chunk) to the Clean Contacts api for matching
    * This step (and associated variable steps) should not be changed
![job and view](images/flow-submit-chunks.png)
* **Wait for processing (matching) to finish**
![job and view](images/flow-wait-for-processing-to-finish.png)
    * This step (and associated variable steps) should not be changed
* **Get match report**
    * This step (and associated variable steps) should not be changed
    * Output variables from the match report that can be used in further flow steps:
        * OutputJobInputQuantity : Total number of rows submitted for cleaning (integer)
        * OutputPassedAwayQuantity : Number of rows matching the 'passed away' service (string)
        * OutputMovedAwayQuantity : Number of rows matching the 'moved away' service (string)
        * OutputMovedToQuantity : Number of rows matching the 'moved to' service (string)
    * Notes:
        * the service quantities are strings because:
            * when a service is not requested the service quantity variable will be blank
            * if the service was requested then the service quantity variable will hold the number of matches (which could be 0)
        * the variables are prefixed with Output to denote that they are output from the job report process (as opposed to input variables) 
![job and view](images/flow-get-match-report.png)
* **Teams message**
    * This step is optional
    * It uses the output variables from the match report step
![job and view](images/flow-teams-message.png)
* **Approval**
    * This step is optional
    * It uses the output variables from the match report step
    * [How to remove the approval step from the Clean Contacts Flow](flow-remove-approval.md)
![job and view](images/flow-approval.png)
* ### **Update matching Dynamics rows** ###
    * Updates Dynamics [match columns](solution-content.md) with the match date (today)
    * The image below shows the update steps without the approval step
![job and view](images/flow-update-dynamics-with-match-results.png)

