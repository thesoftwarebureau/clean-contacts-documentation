![Logo](images/logo.png)

# Customise Flow View #

## Overview ##
* Out of the box, Clean Contacts ships with a single flow configured to clean the contacts table
* It can be customised to clean the [leads table](flow-leads.md) and [accounts table](flow-accounts.md) very easily, as we ship a view defining the input fields required for the leads and accounts table
* You cannot edit the views that ship with Clean Contacts, instead you need to create a new system view

## View Requirements ##
* A new view:
    * must be a system view (not a user view ie. a user filter)
    * have the first column being the 'name' column (this is included by default when creating a system view)
    * all columns after the name will be treated as address columns
    * must have at least one line of address

## How to create a new view ##

### Create a new system view ###
* From Dynamics
    1. Click the 'Settings' cog icon
    2. Select 'Advanced Settings' ![customise flow view](images/flow-custom-table-1.png)
* Select:
    1. 'Settings'
    2. 'Customization'
    ![customise flow view](images/flow-custom-table-2.png)
* Select
    1. 'Customize the System' ![customise flow view](images/flow-custom-table-3.png)
* In the new window that opens: expand the entity/table node ![customise flow view](images/flow-custom-table-4.png)
* Scroll to the table where you want to create the new view:
    1. Expand the entity/table node (customer in this example)
    2. Select the 'Views' Node
    3. Click 'New' to create a new customer View
    ![customise flow view](images/flow-custom-table-5.png)
* In the new window that opens:
    1. Name the view ('Clean Contacts Input Customers' in this example)
    2. Note: the view name can contain alphanumeric characters and/or following special characters  - . ' , / & # ( ) : only
    2. Click OK ![customise flow view](images/flow-custom-table-6.png)
* In the new window that opens: click 'Try the new experience' ![customise flow view](images/flow-custom-table-7.png)
* In the new window that opens: add your address lines so the final view looks like this: 
 ![customise flow view](images/flow-custom-table-8.png)

* Add a filter to the view:
    * Here is the default view filter logic:
    ![customise flow view](images/flow-custom-table-8b.png)
    Exclusion logic in English:
        * Do not clean a record if it has been flagged as 'passed away'
        * Do not clean a record if it has been flagged as 'moved away'
        * Do not clean a record if it has been flagged as 'moved to'
        * Do not clean a record if it has been excluded from cleaning
        * Do not clean a record if it has no full name
        * Do not clean a record if it has no address line 1
        * Do not clean a record if it has been deactivated (disqualified for leads)

* It is important to note:
    * The first column must be the name
    * The remaining columns added will be treated as address lines
    * Use individual columns rather than composite columns
    * There must be at least one line of address

* Finally:
    1. 'Save'
    2. 'Publish' (the view must be published for the flow to be able to access it.) ![customise flow view](images/flow-custom-table-9.png)
* Close all the windows that opened


### How to customise the flow to use a different View ##
* Edit the flow
* Click the step: 'Create Clean Contacts Job and Validate View'
* Change InputViewName from: 'Clean Contacts Input Contacts' to 'Clean Contacts Input Customers' ![customise flow view](images/customise-flow-view.png)
* [Be aware if you are running multiple flows](flow-multiple.md)


### Notes ###
* We only include custom columns with the contact, account and lead tables
    * ref: [Solution Contents](solution-content.md)
