![Logo](images/logo.png)

# Solution Contents #

Clean Contacts comprises of 2 solutions:

* **Clean Contacts Solution**
    * Contains the main flow which performs the cleaning
    * all other required artefacts e.g. plugin
* **Clean Contacts Sales Hub Solution**
    * Contains UI customisation for the sales hub
    * Dashboards, Detail form additions
    * Note that the Sales Hub solution depends upon the main solution

## Clean Contacts Solution ##
* Main flow:
    * [Clean Contacts Flow](flow.md)
* Contact fields/columns:
    ![customise-service](images/contact-columns.png)
* Contact view
    * 'Clean Contacts Input Contacts' which defines the input that will be processed by the Clean Contacts flow when cleaning the contacts table, deactivated records are excluded by default
* Account view
    * 'Clean Contacts Input Accounts' which defines the input that will be processed by the Clean Contacts flow when cleaning the accounts table, deactivated records are excluded by default
* Lead view
    * 'Clean Contacts Input Leads' which defines the input that will be processed by the Clean Contacts flow when cleaning the leads table, disqualified records are excluded by default
* Plugin/Processes
    * The interface between the flow and the Clean Contacts api which performs the cleaning

## Clean Contacts Sales Hub Solution ##
* This solution depends upon Clean Contacts Solution
* All customisation is applied to contact, account and lead tables
* Sales Hub customisation
    * Dashboards (and respective views)
    * Menu additions
    * Detail form with new table for 'Clean Contacts'
        * Shows matching information
    * [Flow to help updating 'Moved To' contact address](moved-to-flow.md)
        * 'Update Contact Moved To Address'
