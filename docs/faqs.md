![Logo](images/logo.png)

# FAQs #

* Where is the Clean Contacts portal?
    * [https://portal.clean-contacts.co.uk](https://portal.clean-contacts.co.uk)
* I've lost my client id
    * log into the [portal](https://portal.clean-contacts.co.uk), your client id is on the home page
* I've lost my client secret
    * log into the [portal](https://portal.clean-contacts.co.uk), from the home page; click re-generate client secret
    * [update client secret in solution config](solution-config.md)
* Where can I download the latest clean contacts solution?
    * log into the [portal](https://portal.clean-contacts.co.uk), from the home page; click 'Download Dynamics Solution'
* How do I check or change the solution configuration?
    * [update solution config](solution-config.md)
* Does running the [passed away](customise-service.md) service delete rows that match as passed away?
    * No, we update the [contact 'passed away match date' column](solution-content.md) for the matching row
* Does running the [moved to](customise-service.md) service overwrite the contact address?
    * No, we update the [contact 'moved to' columns](solution-content.md) for the matching row
* Can I exclude a record from the cleaning process?
    * Yes, the filter logic in the default views excludes records where [Exclude from Cleaning](solution-content.md) is set to Yes
* I have 1000 contacts; why are 800 submitted?
    * Only contacts with a name & at least one line of address are submitted for cleaning
* tsb_clean_contacts_passed_away_match; is this the date of death for the contact?
    * No, it is the date when the match (within the flow) was performed.
* Why is there a nested loop in the flow when submitting data ?
    * Flow loops are currently limited by Microsoft to 5000 iterations
        * Therefore the maximum number of rows that can be submitted = 500 (recs per chunk) * 5000 = 2,500,000 rows
    * But by introducing a nested loop this increases the maximum number of records to: 500 (recs per chunk) * 5000 * 5000 = 12,500,000,000 rows
