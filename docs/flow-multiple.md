![Logo](images/logo.png)

# How to run multiple Clean Contacts flows #

## Overview ##
* The flow uses an environment variable to remember the last record id that was submitted for cleaning.
* When running multiple flows, each flow needs its own environment variable.

## Create a new environment variable ##
* [Create an Environment Variable](solution-config.md) see section: 'Create a New Environment Variable'


## Update the flow to use the new environment variable ##
* ![Set flow environment variable](images/flow-environment-variable.png)


