![Logo](images/logo.png)

# How to clean Accounts #

## How to customise the flow to clean account records ##
* Edit the flow
* Click the step: 'Create Clean Contacts Job and Validate View'
* Edit the step
    1. Change the InputViewName to 'Clean Contacts Input Accounts'
    ![customise-service](images/flow-accounts.png)
* Save the flow
* [Be aware if you are running multiple flows](flow-multiple.md)
