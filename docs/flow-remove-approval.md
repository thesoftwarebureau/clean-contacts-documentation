![Logo](images/logo.png)

# How to remove the approval step from the Clean Contacts Flow #
![flow-remove-approval](images/flow-remove-approval.gif)
